package DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import BASE.DBManager;
import MODELL.torkBeans;


public class torkDAO {
	public static void AddTork(String Tork) {
		/**
		 * 情報登録
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO talk(talkDetail)VALUES(?);";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setString(1,Tork);
			pSTMt.executeUpdate();
			pSTMt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
		return;
	}

	/**
	 * 情報取得
	 */
	public static List<torkBeans> findAll() {
		Connection conn = null;
		List<torkBeans> TorkList = new ArrayList<torkBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// 管理者以外を取得する
			String sql = "SELECT * FROM talk";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String tarkDetail = rs.getString("talkDetail");
				torkBeans TB = new torkBeans(id,tarkDetail);

				TorkList.add(TB);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return TorkList;
	}

}
