package MODELL;

public class torkBeans {
	private int id;
	private String tork;

	public torkBeans(int id, String tork) {
		this.id=id;
		this.tork=tork;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTork() {
		return tork;
	}

	public void setTork(String tork) {
		this.tork = tork;
	}
}
